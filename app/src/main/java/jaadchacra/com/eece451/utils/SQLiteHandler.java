package jaadchacra.com.eece451.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

public class SQLiteHandler extends SQLiteOpenHelper {

    private SQLiteDatabase sqLiteDatabase;

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "android_api";

    // Login table towerid
    private static final String TABLE_TOWER = "tower";
    private static final String TABLE_P2P = "P2P";

    // Login Table Columns towerids
    private static final String KEY_ID = "id";
    private static final String KEY_TOWERID = "towerid";
    private static final String KEY_NETWORKTYPE = "networktype";
    private static final String KEY_OPERATOR = "operator";
    private static final String KEY_SIGNAL= "signal";
    private static final String KEY_LOCATION = "location";


    private static final String KEY_P2P_ID = "id";
    private static final String KEY_MAC = "mac";
    private static final String KEY_NUMBER = "number";
    private static final String KEY_LASTDETECTION = "lastdetection";
    private static final String KEY_LASTINRANGE= "lastinrange";
    private static final String KEY_CUMULATIVEDURATION = "cumulativeduration";



    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TOWERS_TABLE = "CREATE TABLE " + TABLE_TOWER + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TOWERID + " TEXT,"
                + KEY_NETWORKTYPE + " TEXT," + KEY_OPERATOR + " TEXT," + KEY_SIGNAL+ " TEXT,"
                + KEY_LOCATION + " TEXT"
                + ")";
        db.execSQL(CREATE_TOWERS_TABLE);

        String CREATE_P2P_TABLE = "CREATE TABLE " + TABLE_P2P + "("
                + KEY_P2P_ID + " INTEGER PRIMARY KEY," + KEY_MAC + " TEXT,"
                + KEY_NUMBER + " TEXT," + KEY_LASTDETECTION + " TEXT," + KEY_LASTINRANGE+ " TEXT,"
                + KEY_CUMULATIVEDURATION + " TEXT"
                + ")";
        db.execSQL(CREATE_P2P_TABLE);

        Log.d(TAG, "Database tables created");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOWER);

        // Create tables again
        onCreate(db);
    }

    public void onUpgradeP2P(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_P2P);

        // Create tables again
        onCreate(db);
    }

    /**
     * Storing tower details in database
     * */
    public void addTower(String towerid, String networktype, String operator, String signal, String location) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TOWERID, towerid); // Name
        values.put(KEY_NETWORKTYPE, networktype); // Name
        values.put(KEY_OPERATOR, operator); // Email
        values.put(KEY_SIGNAL, signal); // Email
        values.put(KEY_LOCATION, location); // Created At



        // Inserting Row
        long id = db.insert(TABLE_TOWER, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New tower inserted into sqlite: " + id);
    }

    public void addP2P(String mac, String number, String lastdetection, String lastinrange, String cumulativeduration) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_MAC, mac); // Name
        values.put(KEY_NUMBER, number); // Name
        values.put(KEY_LASTDETECTION, lastdetection); // Email
        values.put(KEY_LASTINRANGE, lastinrange); // Email
        values.put(KEY_CUMULATIVEDURATION, cumulativeduration); // Created At



        // Inserting Row
        long id = db.insert(TABLE_P2P, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New P2P inserted into sqlite: " + id);
    }


    public Integer P2Pdelete (int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(TABLE_P2P,"id = ? ", new String[]{Integer.toString(id)});
    }


    public boolean updateP2P ( int id, String mac, String number, String lastdetection, String lastinrange, String cumulativeduration) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_MAC, mac); // Name
        values.put(KEY_NUMBER, number); // Name
        values.put(KEY_LASTDETECTION, lastdetection); // Email
        values.put(KEY_LASTINRANGE, lastinrange); // Email
        values.put(KEY_CUMULATIVEDURATION, cumulativeduration); // Created At
        db.update(TABLE_P2P, values, "id = ? ",  new String[]{Integer.toString(id)} );
        return true;
    }

    public Cursor P2PgetData(String mac) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from P2P where mac="+mac+"", null );
        return res;
    }

    /**
     * Getting tower data from database
     * */
    public ArrayList<HashMap<String, String>> getTowerDetails() {
        ArrayList<HashMap<String, String>> towers = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT  * FROM " + TABLE_TOWER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            HashMap<String, String> tower = new HashMap<>();

            tower.put("towerid", cursor.getString(1));
            tower.put("networktype", cursor.getString(2));
            tower.put("operator", cursor.getString(3));
            tower.put("signal", cursor.getString(4));
            tower.put("location", cursor.getString(5));

            towers.add(tower);
            while(cursor.moveToNext()){
                tower = new HashMap<>();

                tower.put("towerid", cursor.getString(1));
                tower.put("networktype", cursor.getString(2));
                tower.put("operator", cursor.getString(3));
                tower.put("signal", cursor.getString(4));
                tower.put("location", cursor.getString(5));

                towers.add(tower);

            }
        }

        cursor.close();
        db.close();
        // return tower

        return towers;
    }



    /**
     * Getting tower data from database
     * */
    public ArrayList<HashMap<String, String>> getP2PDetails() {
        ArrayList<HashMap<String, String>> P2Ps = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT  * FROM " + TABLE_P2P;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            HashMap<String, String> P2P = new HashMap<>();

            P2P.put("mac", cursor.getString(1));
            P2P.put("number", cursor.getString(2));
            P2P.put("lastdetection", cursor.getString(3));
            P2P.put("lastinrange", cursor.getString(4));
            P2P.put("cumulativeduration", cursor.getString(5));

            P2Ps.add(P2P);
            while(cursor.moveToNext()){
                P2P = new HashMap<>();

                P2P.put("mac", cursor.getString(1));
                P2P.put("number", cursor.getString(2));
                P2P.put("lastdetection", cursor.getString(3));
                P2P.put("lastinrange", cursor.getString(4));
                P2P.put("cumulativeduration", cursor.getString(5));

                P2Ps.add(P2P);

            }
        }

        cursor.close();
        db.close();
        // return P2P

        return P2Ps;
    }


    /**
     * Re crate database Delete all tables and create them again
     * */
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_TOWER, null, null);
        db.close();

        Log.d(TAG, "Deleted all tower info from sqlite");
    }


    /**
     * Re crate database Delete all tables and create them again
     * */
    public void deleteP2Ps() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_P2P, null, null);
        db.close();

        Log.d(TAG, "Deleted all P2P info from sqlite");
    }

}