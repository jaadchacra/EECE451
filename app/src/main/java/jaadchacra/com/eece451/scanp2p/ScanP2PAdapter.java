package jaadchacra.com.eece451.scanp2p;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import jaadchacra.com.eece451.model.P2P;
import jaadchacra.com.eece451.R;


public class ScanP2PAdapter extends RecyclerView.Adapter<ScanP2PAdapter.ViewHolder> {
    private Context context;

    //List to store all superheroes
    List<P2P> statussList;

    //Constructor of this class
    public ScanP2PAdapter(List<P2P> statussList, Context context) {
        super();
        this.statussList = statussList;
        this.context = context;
        Log.d("status", "StudentFeedAdapter");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.list_item_scan_p2p, parent, false);

        Log.d("status", "yo");
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //Getting the particular item from the list
        final P2P status = statussList.get(position);
    }

    @Override
    public int getItemCount() {
        return statussList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views
        public TextView macAddressTextView;
        public TextView phoneNumberTextView;
        public TextView timesDetectedTextView;
        public TextView firstDetectedTextView;
        public TextView lastDetectedTextView;


        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            macAddressTextView = (TextView) itemView.findViewById(R.id.macAddressTextView);
            phoneNumberTextView = (TextView) itemView.findViewById(R.id.phoneNumberTextView);
            timesDetectedTextView = (TextView) itemView.findViewById(R.id.timesDetectedTextView);
            firstDetectedTextView = (TextView) itemView.findViewById(R.id.firstDetectedTextView);
            lastDetectedTextView = (TextView) itemView.findViewById(R.id.lastDetectedTextView);
        }
    }
}