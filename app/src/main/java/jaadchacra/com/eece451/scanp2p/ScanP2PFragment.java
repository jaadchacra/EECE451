package jaadchacra.com.eece451.scanp2p;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.CellInfo;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import jaadchacra.com.eece451.R;
import jaadchacra.com.eece451.jawadfiles.ClientActivity;
import jaadchacra.com.eece451.model.P2P;


public class ScanP2PFragment extends Fragment {

    View view;
//    private RecyclerView recyclerView;
//    private RecyclerView.Adapter studentFeedAdapter;
//    private List<P2P> scanList;
    Button wifiP2P;
    String phoneNumber;

//    ArrayList<CellInfo> cellInfoList;
    private static final String[] LOCATION_PERMS={
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final String[] READ_PERMS={
            Manifest.permission.READ_PHONE_STATE
    };

    private static final int INITIAL_REQUEST=1337;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_scan_p2p, container, false);

        wifiP2P = (Button) view.findViewById(R.id.wifiP2P);

        wifiP2P.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ClientActivity.class);
                //Used so we don't lose them, DiscountCropActivity sends them back here so we upload to server
                intent.putExtra("phoneNumber", phoneNumber);
                startActivity(intent);
            }
        });

//        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
//        recyclerView.setHasFixedSize(true);
//
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        recyclerView.setLayoutManager(linearLayoutManager);
//
//        scanList = new ArrayList<>();
//        populateDummyStatus();
//
//        Log.d("offer", "StudentFeedFragment");
//        studentFeedAdapter = new ScanP2PAdapter(scanList, getActivity());
//        recyclerView.setAdapter(studentFeedAdapter);

        myPhoneNumber();

        return view;
    }

    private void myPhoneNumber() {
        requestPermissions(LOCATION_PERMS, INITIAL_REQUEST);
        requestPermissions(READ_PERMS, INITIAL_REQUEST);

        //Network operator
        TelephonyManager manager = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);



        phoneNumber = manager.getLine1Number();
    }


//    public void populateDummyStatus() {
//        scanList = new ArrayList<>();
//        P2P status1 = new P2P();
//        P2P status2 = new P2P();
//        P2P status3 = new P2P();
//        P2P status4 = new P2P();
//
//
//        scanList.add(status1);
//        scanList.add(status2);
//        scanList.add(status3);
//        scanList.add(status4);
//    }
}
