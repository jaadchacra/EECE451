package jaadchacra.com.eece451.scanp2p;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.peak.salut.Callbacks.SalutCallback;
import com.peak.salut.Callbacks.SalutDataCallback;
import com.peak.salut.Callbacks.SalutDeviceCallback;
import com.peak.salut.Salut;
import com.peak.salut.SalutDataReceiver;
import com.peak.salut.SalutDevice;
import com.peak.salut.SalutServiceData;

import jaadchacra.com.eece451.R;

public class NearbyActivity extends AppCompatActivity implements SalutDataCallback, View.OnClickListener {

    public static final String TAG = "SalutTestApp";
    public SalutDataReceiver dataReceiver;
    public SalutServiceData serviceData;
    public Salut network;
    public Button hostingBtn;
    public Button discoverBtn;
    SalutDataCallback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby);

        hostingBtn = (Button) findViewById(R.id.hostingBtn);
        discoverBtn = (Button) findViewById(R.id.discoverBtn);

        hostingBtn.setOnClickListener(this);
        discoverBtn.setOnClickListener(this);
        
        
        /*Create a data receiver object that will bind the callback
        with some instantiated object from our app. */
        dataReceiver = new SalutDataReceiver(this, this);


        /*Populate the details for our awesome service. */
        serviceData = new SalutServiceData("testAwesomeService", 60606, "HOST");

        /*Create an instance of the Salut class, with all of the necessary data from before.
        * We'll also provide a callback just in case a device doesn't support WiFi Direct, which
        * Salut will tell us about before we start trying to use methods.*/
        network = new Salut(dataReceiver, serviceData, new SalutCallback() {
            @Override
            public void call() {
                // wiFiFailureDiag.show();
                // OR
                Log.e(TAG, "Sorry, but this device does not support WiFi Direct.");
            }
        });

    }

    //HOST
    private void setupNetwork()
    {
        if(!network.isRunningAsHost)
        {
            //When a device connects and is successfully registered, this callback will be fired. 
            //You can access the entire list of registered clients using the field registeredClients
            network.startNetworkService(new SalutDeviceCallback() {
                @Override
                public void call(SalutDevice salutDevice) {
                    Toast.makeText(getApplicationContext(), "Device: " + salutDevice.instanceName + " connected.", Toast.LENGTH_SHORT).show();
                }
            });

            hostingBtn.setText("Stop Service");
            discoverBtn.setVisibility(View.INVISIBLE);
            discoverBtn.setClickable(false);
        }
        else
        {
            network.stopNetworkService(false);
            hostingBtn.setText("Start Service");
            discoverBtn.setVisibility(View.VISIBLE);
            discoverBtn.setClickable(true);
        }
    }

    //CLIENT
    private void discoverServices()
    {
        if(!network.isRunningAsHost && !network.isDiscovering)
        {
            //Salut will only connect to found services of the same type.
            network.discoverNetworkServices(new SalutCallback() {
                @Override
                public void call() {
                    Toast.makeText(getApplicationContext(), "Device: " + network.foundDevices.get(0).instanceName + " found.", Toast.LENGTH_SHORT).show();
                }
            }, true);
            discoverBtn.setText("Stop Discovery");
            hostingBtn.setVisibility(View.INVISIBLE);
            hostingBtn.setClickable(false);
        }
        else
        {
            //the framework will continue to discover services until you manually call stopServiceDiscovery()
            network.stopServiceDiscovery(true);
            discoverBtn.setText("Discover Services");
            hostingBtn.setVisibility(View.VISIBLE);
            hostingBtn.setClickable(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_base, menu);
        return true;
    }

    /*Create a callback where we will actually process the data.*/
    @Override
    public void onDataReceived(Object o) {
        //Data Is Received
    }

    @Override
    public void onClick(View v) {

        if(!Salut.isWiFiEnabled(getApplicationContext()))
        {
            Toast.makeText(getApplicationContext(), "Please enable WiFi first.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(v.getId() == R.id.hostingBtn)
        {
            setupNetwork();
        }
        else if(v.getId() == R.id.discoverBtn)
        {
            discoverServices();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

//        if(MyApp.isHost)
//            network.stopNetworkService(true);
//        else
//            network.unregisterClient(null);
    }
}


