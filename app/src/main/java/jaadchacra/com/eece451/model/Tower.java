package jaadchacra.com.eece451.model;

/**
 * Created by ATOM on 4/30/2017.
 */

public class Tower {
    String towerid;
    String networktype;
    String operator;
    String signal;
    String location;

    public String getTowerid() {
        return towerid;
    }

    public void setTowerid(String towerid) {
        this.towerid = towerid;
    }

    public String getNetworktype() {
        return networktype;
    }

    public void setNetworktype(String networktype) {
        this.networktype = networktype;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


}
