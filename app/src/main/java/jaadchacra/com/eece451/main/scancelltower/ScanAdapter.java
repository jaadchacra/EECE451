package jaadchacra.com.eece451.main.scancelltower;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import jaadchacra.com.eece451.R;
import jaadchacra.com.eece451.model.Tower;


public class ScanAdapter extends RecyclerView.Adapter<ScanAdapter.ViewHolder> {
    private Context context;

    //List to store all superheroes
    List<Tower> towersList;

    //Constructor of this class
    public ScanAdapter(List<Tower> towersList, Context context) {
        super();
        this.towersList = towersList;
        this.context = context;
        Log.d("status", "StudentFeedAdapter");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.list_item_scan, parent, false);

        Log.d("status", "yo");
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //Getting the particular item from the list
        final Tower tower = towersList.get(position);
        holder.towerIDTextView.setText(tower.getTowerid());
        holder.networkTypeTextView.setText(tower.getNetworktype());
        holder.signalTextView.setText(tower.getSignal());
        holder.locationTextView.setText(tower.getLocation());
        holder.operatorTextView.setText(tower.getOperator());

    }

    @Override
    public int getItemCount() {
        return towersList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views
        public TextView towerIDTextView;
        public TextView networkTypeTextView;
        public TextView signalTextView;
        public TextView locationTextView;
        public TextView operatorTextView;


        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            towerIDTextView = (TextView) itemView.findViewById(R.id.towerIDTextView);
            networkTypeTextView = (TextView) itemView.findViewById(R.id.networkTypeTextView);
            signalTextView = (TextView) itemView.findViewById(R.id.signalTextView);
            locationTextView = (TextView) itemView.findViewById(R.id.locationTextView);
            operatorTextView = (TextView) itemView.findViewById(R.id.operatorTextView);
        }
    }
}