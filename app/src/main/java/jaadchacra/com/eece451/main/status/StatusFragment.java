package jaadchacra.com.eece451.main.status;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import org.w3c.dom.Text;

import java.util.Set;

import jaadchacra.com.eece451.R;
import myutils.BluetoothChangeReceiver;
import myutils.BluetoothConnectionChangeReceiver;
import myutils.NetworkChangeReceiver;
import myutils.WifiChangeReceiver;

public class StatusFragment extends Fragment {
    View view;
    Bundle mSavedInstanceState;
    ImageView bluetoothImageView, wifiImageView, cellularImageView;
    Button statusButton;
    private BluetoothChangeReceiver myBluetoothChangeReceiver;
    private WifiChangeReceiver myWifiChangeReceiver;
    private BluetoothConnectionChangeReceiver myBluetoothConnectionChangeReceiver;
    private NetworkChangeReceiver myNetworkChangeReceiver;
    @Override
    public void onResume() {
        super.onResume();
        // put your code here...
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (isVisibleToUser) {

            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_status, container, false);
        //This is basically never used, it's just to use getData from onScrolled function
        mSavedInstanceState = savedInstanceState;

        init(view);

        // Intent Filter (decides if wifi, bluetooth, or cellular)
        IntentFilter wifiFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        // Registering Receiver to Activity, we can register as many as we want
        getActivity().registerReceiver(myWifiChangeReceiver, wifiFilter);

        IntentFilter bluetoothFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        getActivity().registerReceiver(myBluetoothChangeReceiver, bluetoothFilter);

        IntentFilter bluetoothConnectionFilter = new IntentFilter();
        bluetoothConnectionFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        bluetoothConnectionFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        getActivity().registerReceiver(myBluetoothConnectionChangeReceiver, bluetoothConnectionFilter);

        IntentFilter networkFilter = new IntentFilter();
        networkFilter.addAction(android.telephony.TelephonyManager.ACTION_PHONE_STATE_CHANGED);
        getActivity().registerReceiver(myNetworkChangeReceiver, networkFilter);

        checkStatus();

        statusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vieww) {
                checkStatus();
            }
        });

        return view;
    }

    public void init(View view){
        bluetoothImageView = (ImageView) view.findViewById(R.id.bluetoothImageView);
        wifiImageView = (ImageView) view.findViewById(R.id.wifiImageView);
        cellularImageView = (ImageView) view.findViewById(R.id.cellularImageView);
        statusButton = (Button) view.findViewById(R.id.statusButton);

        myBluetoothChangeReceiver = new BluetoothChangeReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                checkStatus();
            }
        };
        myWifiChangeReceiver = new WifiChangeReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                checkStatus();
            }
        };
        myBluetoothConnectionChangeReceiver = new BluetoothConnectionChangeReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                checkStatus();
            }
        };
        myNetworkChangeReceiver = new NetworkChangeReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                checkStatus();
            }
        };
    }

    public void checkStatus(){
        checkCellularStatus();
        checkWifiStatus();
        checkBluetoothStatus();
    }
    public void checkCellularStatus(){
        ConnectivityManager connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        //Network operator
        TelephonyManager manager = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String carrierName = manager.getNetworkOperatorName();
        Log.d("carrierName", "+"+carrierName);
        if(carrierName == "" || carrierName == null) {
            replaceImage("cellular_off", cellularImageView, getContext());
            Log.d("carrierName", "cellular_off"+carrierName);
        } else{
            replaceImage("cellular_connected", cellularImageView, getContext());
        }
//        if ( connManager.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||  connManager .getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING  ) {
//            replaceImage("cellular_connected", cellularImageView, getContext());
//            Log.d("carrierName", "CONNECTED"+carrierName);
//        }
    }
    public void checkBluetoothStatus() {
        //bluetooth  enabled
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            replaceImage("bluetooth_off", bluetoothImageView, getContext());
        } else {
            if (mBluetoothAdapter.isEnabled()) {
                // Bluetooth is  enable :)
                replaceImage("bluetooth_on", bluetoothImageView, getContext());
            } else{
                replaceImage("bluetooth_off", bluetoothImageView, getContext());
            }
        }
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0)
            replaceImage("bluetooth_connected", bluetoothImageView, getContext());
    }
    public void checkWifiStatus() {
        //Wifi on
        WifiManager wifi = (WifiManager)getContext().getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled()){
            replaceImage("wifi_on", wifiImageView, getContext());
        } else{
            replaceImage("wifi_off", wifiImageView, getContext());
        }

        //Wifi connected
        ConnectivityManager connManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (mWifi.isConnected()) {
            replaceImage("wifi_connected", wifiImageView, getContext());
        }
    }

    public static void replaceImage(String imageName, ImageView imageView, Context context) {
        String uri = "@drawable/" + imageName;  // myresource (without the extension) is the image
        int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
        Drawable res = context.getResources().getDrawable(imageResource);
        imageView.setImageDrawable(res);
    }
}

