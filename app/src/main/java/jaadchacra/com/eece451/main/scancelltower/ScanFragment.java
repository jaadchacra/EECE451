package jaadchacra.com.eece451.main.scancelltower;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.CellInfo;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import jaadchacra.com.eece451.R;
import jaadchacra.com.eece451.model.Tower;
import jaadchacra.com.eece451.utils.SQLiteHandler;
import myutils.CSVWriter;

public class ScanFragment extends Fragment {
    View view;
    Bundle mSavedInstanceState;
    TextView scanButton;
    ArrayList<CellInfo> cellInfoList;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter studentFeedAdapter;
    private List<Tower> scanList;
    private SQLiteHandler db;
    Button exportTableButton;

    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final String[] READ_PERMS = {
            Manifest.permission.READ_PHONE_STATE
    };
    private static final int INITIAL_REQUEST = 1337;

    @Override
    public void onResume() {
        super.onResume();
        // put your code here...
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (isVisibleToUser) {

            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_scan, container, false);
        //This is basically never used, it's just to use getData from onScrolled function
        mSavedInstanceState = savedInstanceState;

        scanButton = (TextView) view.findViewById(R.id.scanButton);
        scanButtonClickListener();

        exportTableButton = (Button) view.findViewById(R.id.exportTableButton);
        exportTableButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exportsqlite();
            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        scanList = new ArrayList<>();
        updateList();

        Log.d("offer", "StudentFeedFragment");
        studentFeedAdapter = new ScanAdapter(scanList, getActivity());
        recyclerView.setAdapter(studentFeedAdapter);

        return view;
    }

    private void updateList() {
        db = new SQLiteHandler(getActivity().getApplicationContext());

        ArrayList<HashMap<String, String>> arrayHashTowers = db.getTowerDetails();
        for (int i = 0; i < arrayHashTowers.size(); i++) {
            HashMap<String, String> hashTower = arrayHashTowers.get(i);
            Tower tower = new Tower();

            tower.setTowerid(hashTower.get("towerid"));
            tower.setNetworktype(hashTower.get("networktype"));
            tower.setOperator(hashTower.get("operator"));
            tower.setSignal(hashTower.get("signal"));
            tower.setLocation(hashTower.get("location"));

            scanList.add(tower);
        }
    }


    public void scan(String location) {

        try {
            requestPermissions(LOCATION_PERMS, INITIAL_REQUEST);
            requestPermissions(READ_PERMS, INITIAL_REQUEST);

            //Network operator
            TelephonyManager manager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            String carrierName = manager.getNetworkOperatorName();
            cellInfoList = (ArrayList) manager.getAllCellInfo();

            String cellInfo = "";
            for (int i = 0; i < cellInfoList.size(); i++) {
                cellInfo = cellInfoList.get(i).toString();
                Log.d("testststststststs", "cellInfo: " + i + " " + cellInfo);
                if (cellInfo.substring(cellInfo.indexOf("mRegistered=") + 12, cellInfo.indexOf(" mTimeStampType=")).equals("YES")) {
                    break;
                }
            }


            Tower tower = new Tower();
            tower.setNetworktype(cellInfo.substring(cellInfo.indexOf("CellInfo") + 8, cellInfo.indexOf(':')));
            if (tower.getNetworktype().equals("Wcdma"))
                tower.setTowerid(cellInfo.substring(cellInfo.indexOf("mCid=") + 5, cellInfo.indexOf(" mPsc=")));
            else
                tower.setTowerid(cellInfo.substring(cellInfo.indexOf("mCid=") + 5, cellInfo.indexOf("} Ce")));
            tower.setOperator(carrierName);
            tower.setSignal(cellInfo.substring(cellInfo.indexOf("ss=") + 3, cellInfo.indexOf(" ber=")));
            tower.setLocation(location);

            db.addTower(tower.getTowerid(), tower.getNetworktype(), carrierName, tower.getSignal(), location);
            scanList.add(tower);

            studentFeedAdapter.notifyDataSetChanged();
        } catch (Exception e){
            Toast.makeText(getActivity(), "TelephonyManager.getAllCellInfo is disabled on your phone", Toast.LENGTH_SHORT).show();
        }
    }


    private void scanButtonClickListener() {
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText txtUrl = new EditText(getContext());
                txtUrl.setHint("ex: Hamra");
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setView(txtUrl)
                        .setTitle("Your Location")
                        .setMessage("Enter Your Location")
                        .setCancelable(true)
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        scan(txtUrl.getText().toString());
                        dialog.cancel();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    final String SAMPLE_DB_NAME = "android_api.db";//database name

    public void exportsqlite() {
        // TODO Auto-generated method stub


        File dbFile = getActivity().getDatabasePath("android_api.db");
        File exportDir = new File(Environment.getExternalStorageDirectory(), "JJJS.csv");

        Log.d("paththththtt", exportDir+"");
        if (!exportDir.exists()) {
            exportDir.mkdirs();
        }

        File file = new File(exportDir, "csvname.csv");
        try {
            Toast.makeText(getActivity(), "Your database has been exported", Toast.LENGTH_LONG).show();

            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));

            SQLiteDatabase mydb = db.getReadableDatabase();
            Cursor curCSV = mydb.rawQuery("SELECT * FROM tower", null);
            csvWrite.writeNext(curCSV.getColumnNames());
            while (curCSV.moveToNext()) {
                //Which column you want to exprort
                String arrStr[] = {curCSV.getString(0), curCSV.getString(1), curCSV.getString(2)};
                csvWrite.writeNext(arrStr);
            }
            csvWrite.close();
            curCSV.close();
        } catch (Exception sqlEx) {
            Log.e("MainActivity", sqlEx.getMessage(), sqlEx);
        }

//        File sd = Environment.getExternalStorageDirectory();
//        File data = Environment.getDataDirectory();
//        FileChannel source = null;
//        FileChannel destination = null;
//
//        File currentDB = getActivity().getDatabasePath(SAMPLE_DB_NAME);
//
//        Log.d("curenttttDb", currentDB"+");
//
//        String backupDBPath = SAMPLE_DB_NAME;
//        File backupDB = new File(sd, backupDBPath);
//        try {
//            source = new FileInputStream(currentDB).getChannel();
//            destination = new FileOutputStream(backupDB).getChannel();
//            destination.transferFrom(source, 0, source.size());
//            source.close();
//            destination.close();
//            Toast.makeText(getActivity(), "Your database has been exported", Toast.LENGTH_LONG).show();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


    }


}

