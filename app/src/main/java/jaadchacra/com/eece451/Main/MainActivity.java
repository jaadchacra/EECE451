package jaadchacra.com.eece451.main;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

import jaadchacra.com.eece451.R;
import jaadchacra.com.eece451.main.scancelltower.ScanFragment;
import jaadchacra.com.eece451.main.status.StatusFragment;
import jaadchacra.com.eece451.scanp2p.ScanP2PFragment;

public class MainActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }


    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        Bundle bundle3 = new Bundle();
        ScanP2PFragment sales3 = new ScanP2PFragment();
        sales3.setArguments(bundle3);
        adapter.addFragment(sales3, "ScanP2PFragment");

        Bundle bundle1 = new Bundle();
        StatusFragment about = new StatusFragment();
        about.setArguments(bundle1);
        adapter.addFragment(about, "Status");

        Bundle bundle = new Bundle();
        ScanFragment sales = new ScanFragment();
        sales.setArguments(bundle);
        adapter.addFragment(sales, "Scan");



        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
