package jaadchacra.com.eece451.jawadfiles;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;

public class ConnStates {
    private static String state;
    protected static int RSSI;
    protected static String mDeviceName = "";

    public ConnStates(final TelephonyManager tm){
        PhoneStateListener phoneStateListener = new PhoneStateListener() {
            @Override
            public void onSignalStrengthsChanged(SignalStrength signalStrength) {
                super.onSignalStrengthsChanged(signalStrength);
                int SignalStrength = signalStrength.getGsmSignalStrength();
                SignalStrength = (2 * SignalStrength) - 113; // -> dBm
                state = "Cellular network: " + tm.getSimOperatorName().toString() + ": " + SignalStrength + "db";
                Log.d(ClientActivity.TAG, "state: " + state);
            }
        };
        tm.listen(phoneStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
    }

}