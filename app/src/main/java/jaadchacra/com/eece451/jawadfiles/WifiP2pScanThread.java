package jaadchacra.com.eece451.jawadfiles;

import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.util.Log;

public class WifiP2pScanThread extends Thread {

    private WifiP2pManager manager;
    private Channel channel;
    private DevicesTable fragment;
    private boolean scan = true;
    private int period = 15000; // wait for 15s after each scan
    protected static int createdThreads = 0;
    protected static int stoppedThreads = 0;


    public WifiP2pScanThread(WifiP2pManager manager, Channel channel, DevicesTable fragment) {
        this.manager = manager;
        this.channel = channel;
        this.fragment = fragment;
    }

    @Override
    public void run() {
        Log.d(ClientActivity.TAG, "Total created threads: " + ++createdThreads);
        while (scan) {
            fragment.onInitiateDiscovery();
            manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    if (fragment != null) {
                        fragment.clearPeers();
                    }
                    Log.d(ClientActivity.TAG, "Scan number: " + ++ClientActivity.numberofScans + " started");
                }

                @Override
                public void onFailure(int reasonCode) {

                    /*Toast.makeText(ClientActivity.this, "Discovery Failed : " + reasonCode,
                            Toast.LENGTH_SHORT).show();*/
                }
            });
            try {
                Thread.sleep(period);
            } catch (InterruptedException e) {
                Log.e(ClientActivity.TAG, "Thread unable to sleep:\n" + e.getMessage());
            }
        }
    }

    public void stopScan() {
        Log.d(ClientActivity.TAG, "Thread stopped. Total stopped threads: " + ++stoppedThreads + ", Number of total created: " + createdThreads);
        scan = false;
    }

    public boolean isScanning() {
        return scan;
    }
}