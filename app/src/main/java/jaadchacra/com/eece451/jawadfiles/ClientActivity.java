package jaadchacra.com.eece451.jawadfiles;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import jaadchacra.com.eece451.R;

public class ClientActivity extends AppCompatActivity implements WifiP2pManager.ChannelListener, DevicesTable.DeviceActionListener {

    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final String[] READ_PERMS = {
            Manifest.permission.READ_PHONE_STATE
    };
    private static final int INITIAL_REQUEST = 1337;

    public static final String TAG = "wifi-p2p";
    protected WifiP2pManager manager;
    private TelephonyManager tm;
    private boolean isWifiP2pEnabled = false;
    private boolean retryChannel = false;
    protected static DevicesTable fragment;
    protected WifiP2pScanThread wifiP2pScanThread;
    private final float txtSize = 16;
    protected static String phoneNumber = "";
    protected static String mac = "";
    protected static String myphoneNumber = "";


    private final IntentFilter intentFilter = new IntentFilter();
    protected WifiP2pManager.Channel channel;
    private BroadcastReceiver receiver = null;
    private TableLayout table;
    protected static WifiP2pDevice device;

    protected static Context context;
    private ProgressBar prog;
    protected static int numberofScans = 0;

    protected static Map<String, String[]> detectedDevices; //MAC is the key. These are the values: in-range, nickname, name, time range, #times detected, cumulative duration of detection, last time detected, last time range, currTimeMilli1, currTimeMilli2, id
    //protected static Map<String, String[]> onlineDevices;
    protected static Set<String> devsOn;
    private Menu menu;
    private WebView wv;
    protected static File traceFile;
    protected String uploadFileName;
//    protected static final String upLoadServerUri = "http://wifip2p.3eeweb.com/processfile.php?id=" + phoneNumber + ".txt";
    private int size = 11;

    /**
     * @param isWifiP2pEnabled the isWifiP2pEnabled to set
     */
    public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
        this.isWifiP2pEnabled = isWifiP2pEnabled;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        tm = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
        super.onCreate(savedInstanceState);


        myphoneNumber = getIntent().getStringExtra("phoneNumber");

        setContentView(R.layout.content_client);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        detectedDevices = new TreeMap<>();
        //onlineDevices = new TreeMap<>();
        devsOn = new TreeSet<>();
        load();
        init(detectedDevices);
        getPhoneNumber();
        context = this;
        prog = (ProgressBar) findViewById(R.id.progressBar);
        prog.setVisibility(View.GONE);
        wifiP2pScanThread = new WifiP2pScanThread(manager, channel, fragment);

        // add necessary intent values to be matched.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
        intentFilter.addAction(TelephonyManager.EXTRA_STATE);
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);

        manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(this, getMainLooper(), null);
    }

    /**
     * register the BroadcastReceiver with the intent values to be matched
     */
    @Override
    public void onResume() {
        super.onResume();
        receiver = new WiFiDirectBroadcastReceiver(manager, channel, this);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        fragment = (DevicesTable) getFragmentManager()
                .findFragmentById(R.id.frag_devs);
        fragment.save();
    }

    /**
     * Remove all peers and clear all fields. This is called on
     * BroadcastReceiver receiving a state change event.
     */
    public void resetData() {
        DevicesTable fragmentList = (DevicesTable) getFragmentManager()
                .findFragmentById(R.id.frag_devs);
        if (fragmentList != null) {
            fragmentList.clearPeers();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_client, menu);
        return true;
    }

    /*
     * (non-Javadoc)
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                wv.setVisibility(View.GONE);
                fragment = (DevicesTable) getFragmentManager()
                        .findFragmentById(R.id.frag_devs);
                fragment.getView().setVisibility(View.VISIBLE);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                return true;

            case R.id.btn_direct_start:
                if (!isWifiP2pEnabled) {
                    Toast.makeText(ClientActivity.this, R.string.p2p_off_warning,
                            Toast.LENGTH_SHORT).show();
                    Log.d(ClientActivity.TAG, "Wifi p2p not enabled");
                    return true;
                }
                fragment = (DevicesTable) getFragmentManager()
                        .findFragmentById(R.id.frag_devs);
                item.setVisible(false);
                MenuItem m = menu.findItem(R.id.btn_direct_stop);
                m.setVisible(true);
                startScan();
                Toast.makeText(ClientActivity.this,
                        "Discovering peers",
                        Toast.LENGTH_SHORT).show();
                return true;
            case R.id.btn_direct_stop:
                item.setVisible(false);
                MenuItem m1 = menu.findItem(R.id.btn_direct_start);
                m1.setVisible(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    manager.stopPeerDiscovery(channel, new WifiP2pManager.ActionListener() {
                        @Override
                        public void onSuccess() {
                            Iterator<String> i = detectedDevices.keySet().iterator();
                            while (i.hasNext()) { // device went from online to offline
                                String mac = i.next();
                                String[] values = detectedDevices.get(mac);
                                values[0] = "Out of range";
                                detectedDevices.put(mac, values);
                                updateRow(detectedDevices, mac, Integer.parseInt(values[10]));
                            }
                        }

                        @Override
                        public void onFailure(int reason) {

                        }
                    });
                }
                stopScan();
                Toast.makeText(ClientActivity.this,
                        "Peer discovery stopped",
                        Toast.LENGTH_SHORT).show();
                return true;
            case R.id.btn_export:
                    Toast.makeText(this, "Table copied to /storage/emulated/0/Android/data/jaadchacra.com.eece451/files/p2p-info.txt", Toast.LENGTH_SHORT).show();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void exportTable() throws IOException {
        String sourcePath = this.getExternalFilesDir(null).getAbsolutePath() + "p2p-info.txt";
        File source = new File(sourcePath);

        String destinationPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download";

        Log.d("destionationnn", destinationPath);
        File destination = new File(destinationPath);
//        try
//        {
//            FileUtils.copyFile(source, destination);
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }

        InputStream in = new FileInputStream(source);
        OutputStream out = new FileOutputStream(destination);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();

    }

    @Override
    public void showDetails(WifiP2pDevice device) {
    }

    @Override
    public void connect(final WifiP2pConfig config) {
    }

    @Override
    public void disconnect() {
    }

    @Override
    public void onChannelDisconnected() {
        // we will try once more
        if (manager != null && !retryChannel) {
            Toast.makeText(this, "Channel lost. Trying again", Toast.LENGTH_LONG).show();
            resetData();
            retryChannel = true;
            manager.initialize(this, getMainLooper(), this);
        } else {
            Toast.makeText(this,
                    "Severe! Channel is probably lost permanently. Try Disable/Re-Enable P2P.",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void cancelDisconnect() {
        /*
         * A cancel abort request by user. Disconnect i.e. removeGroup if
         * already connected. Else, request WifiP2pManager to abort the ongoing
         * request
         */
        if (manager != null) {
            final DevicesTable fragment = (DevicesTable) getFragmentManager()
                    .findFragmentById(R.id.frag_devs);
            if (fragment.getDevice() == null
                    || fragment.getDevice().status == WifiP2pDevice.CONNECTED) {
                disconnect();
            } else if (fragment.getDevice().status == WifiP2pDevice.AVAILABLE
                    || fragment.getDevice().status == WifiP2pDevice.INVITED) {

                manager.cancelConnect(channel, new WifiP2pManager.ActionListener() {

                    @Override
                    public void onSuccess() {
                        Toast.makeText(ClientActivity.this, "Aborting connection",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int reasonCode) {
                        Toast.makeText(ClientActivity.this,
                                "Connect abort request failed. Reason Code: " + reasonCode,
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    public void load() {
        try {
            // Creates a trace file in the primary external storage space of the
            // current application.
            // If the file does not exists, it is created.
            traceFile = new File(this.getExternalFilesDir(null), "p2p-info.txt");
            uploadFileName = traceFile.getAbsolutePath();
            Log.d("absolutePathhhhh", uploadFileName);
            if (!traceFile.exists())
                traceFile.createNewFile();
            // Reads a line from the trace file
            BufferedReader reader = new BufferedReader(new FileReader(traceFile));
            phoneNumber = reader.readLine();
            mac = reader.readLine();
            String[] data = new String[size];
            String input;
            String mac = "";
            for (int i = 0; (input = reader.readLine()) != null; i++) {
                if (i % (size + 1) == 0) {
                    mac = input;
                } else {
                    data[(i % (size + 1)) - 1] = input;
                    if (i % (size + 1) == size) {
                        detectedDevices.put(mac, data);
                        data = new String[size];
                    }
                }
            }
            Log.d(TAG, "File loaded.");
        } catch (IOException e) {
            Log.e(TAG, "Unable to read from the p2p-info.txt file.");
        }
    }

    public void startScan() {
        prog.setVisibility(View.VISIBLE);
        if (wifiP2pScanThread != null && wifiP2pScanThread.isScanning() && wifiP2pScanThread.createdThreads != 0)
            return;
        wifiP2pScanThread = new WifiP2pScanThread(manager, channel, fragment);
        wifiP2pScanThread.start();
    }

    public void stopScan() {
        prog.setVisibility(View.GONE);
        if (wifiP2pScanThread != null) {
            if (wifiP2pScanThread.isScanning())
                wifiP2pScanThread.stopScan();
        }
    }

    public void init(Map map) {
        table = (TableLayout) findViewById(R.id.table_main);
        TableRow tbrow0 = new TableRow(this);
        TextView tvstatus = new TextView(this);
        tvstatus.setText(" Status ");
        tvstatus.setTextColor(Color.WHITE);
        tvstatus.setBackgroundResource(R.drawable.cell_shape);
        tvstatus.setTextSize(txtSize + 1);
        tbrow0.addView(tvstatus);
        TextView tvmac = new TextView(this);
        tvmac.setText(" MAC ");
        tvmac.setTextColor(Color.WHITE);
        tvmac.setBackgroundResource(R.drawable.cell_shape);
        tvmac.setTextSize(txtSize + 1);
        tbrow0.addView(tvmac);
        TextView tvdevicename = new TextView(this);
        tvdevicename.setText(" Device Name ");
        tvdevicename.setTextColor(Color.WHITE);
        tvdevicename.setBackgroundResource(R.drawable.cell_shape);
        tvdevicename.setTextSize(txtSize + 1);
        tbrow0.addView(tvdevicename);
        TextView tvtimerange = new TextView(this);
        tvtimerange.setText(" Time-Range ");
        tvtimerange.setTextColor(Color.WHITE);
        tvtimerange.setBackgroundResource(R.drawable.cell_shape);
        tvtimerange.setTextSize(txtSize + 1);
        tbrow0.addView(tvtimerange);
        TextView tvtimesdetected = new TextView(this);
        tvtimesdetected.setText(" #Times Detected ");
        tvtimesdetected.setTextColor(Color.WHITE);
        tvtimesdetected.setBackgroundResource(R.drawable.cell_shape);
        tvtimesdetected.setTextSize(txtSize + 1);
        tbrow0.addView(tvtimesdetected);
        TextView tvoverall = new TextView(this);
        tvoverall.setText(" Overall Detection ");
        tvoverall.setTextColor(Color.WHITE);
        tvoverall.setBackgroundResource(R.drawable.cell_shape);
        tvoverall.setTextSize(txtSize + 1);
        tbrow0.addView(tvoverall);
        TextView tvlastdetected = new TextView(this);
        tvlastdetected.setText(" Last Detected ");
        tvlastdetected.setTextColor(Color.WHITE);
        tvlastdetected.setBackgroundResource(R.drawable.cell_shape);
        tvlastdetected.setTextSize(txtSize + 1);
        tbrow0.addView(tvlastdetected);
        TextView tvlasttimerange = new TextView(this);
        tvlasttimerange.setText(" Last Time-Range ");
        tvlasttimerange.setTextColor(Color.WHITE);
        tvlasttimerange.setTextSize(txtSize + 1);
        tvlasttimerange.setBackgroundResource(R.drawable.cell_shape);
        tbrow0.addView(tvlasttimerange);
        TextView tvid = new TextView(this);
        tvid.setText(" ID ");
        tvid.setTextColor(Color.WHITE);
        tvid.setTextSize(txtSize + 1);
        tvid.setBackgroundResource(R.drawable.cell_shape);
        tbrow0.addView(tvid);

        TextView tvname = new TextView(this);
        tvname.setText(" Name ");
        tvname.setTextColor(Color.WHITE);
        tvname.setBackgroundResource(R.drawable.cell_shape);
        tvname.setTextSize(txtSize + 1);
        tbrow0.addView(tvname);
        tvname.setVisibility(View.INVISIBLE);

        table.addView(tbrow0);

        updateTable(map, map.keySet());
    }

    public void updateTable(final Map<String, String[]> map, Set<String> keys) {
        for (String KEY : keys) {
            final String key;
            if (KEY.contains("`")) {
                key = KEY.split("`", 2)[1];
            } else {
                key = KEY;
            }
            Log.d(TAG, "tobesorted " + key);
            final String[] values = map.get(KEY);
            TableRow tbrow = new TableRow(this);
            TextView status = new TextView(this);
            status.setText(" " + values[0] + " ");
            status.setTextColor(Color.WHITE);
            status.setGravity(Gravity.CENTER);
            status.setBackgroundResource(R.drawable.cell_shape2);
            status.setTextSize(txtSize);
            tbrow.addView(status);
            TextView mac = new TextView(this);
            mac.setText(" " + key + " ");
            mac.setTextColor(Color.WHITE);
            mac.setGravity(Gravity.CENTER);
            mac.setBackgroundResource(R.drawable.cell_shape2);
            mac.setTextSize(txtSize);
            tbrow.addView(mac);
            TextView tvdevicename = new TextView(this);
            tvdevicename.setText(" " + values[2] + " ");
            tvdevicename.setTextColor(Color.WHITE);
            tvdevicename.setGravity(Gravity.CENTER);
            tvdevicename.setBackgroundResource(R.drawable.cell_shape2);
            tvdevicename.setTextSize(txtSize);
            tbrow.addView(tvdevicename);
            TextView tvtimerange = new TextView(this);
            Long time = Long.parseLong(values[3].split(",", 2)[0]) / 1000;
            tvtimerange.setText(" " + Long.toString((time / 60) % 60) + "m-" + Long.toString((time % 60)) + "s ");
            tvtimerange.setTextColor(Color.WHITE);
            tvtimerange.setGravity(Gravity.CENTER);
            tvtimerange.setBackgroundResource(R.drawable.cell_shape2);
            tvtimerange.setTextSize(txtSize);
            tbrow.addView(tvtimerange);
            TextView tvtimesdetected = new TextView(this);
            tvtimesdetected.setText(" " + values[4] + " ");
            tvtimesdetected.setTextColor(Color.WHITE);
            tvtimesdetected.setGravity(Gravity.CENTER);
            tvtimesdetected.setBackgroundResource(R.drawable.cell_shape2);
            tvtimesdetected.setTextSize(txtSize);
            tbrow.addView(tvtimesdetected);
            TextView tvoverall = new TextView(this);
            Long cumulative = Long.parseLong(values[5].split(",", 2)[0]) / 1000;
            tvoverall.setText(" " + Long.toString((cumulative / 3600) % 24) + "h-" + Long.toString((cumulative / 60) % 60) + "m-" + Long.toString(cumulative % 60) + "s" + " ");
            tvoverall.setTextColor(Color.WHITE);
            tvoverall.setGravity(Gravity.CENTER);
            tvoverall.setBackgroundResource(R.drawable.cell_shape2);
            tvoverall.setTextSize(txtSize);
            tbrow.addView(tvoverall);
            TextView tvlastdetecetd = new TextView(this);
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy 'at' h:mm a");
            Date resultdate = new Date(Long.parseLong(values[6].split(",", 2)[0]));
            tvlastdetecetd.setText(" " + sdf.format(resultdate) + " ");
            tvlastdetecetd.setTextColor(Color.WHITE);
            tvlastdetecetd.setGravity(Gravity.CENTER);
            tvlastdetecetd.setBackgroundResource(R.drawable.cell_shape2);
            tvlastdetecetd.setTextSize(txtSize);
            tbrow.addView(tvlastdetecetd);
            TextView tvlasttimerange = new TextView(this);
            Long timerange = Long.parseLong(values[7]);
            tvlasttimerange.setText(" " + Long.toString((timerange / 60) % 60) + "m-" + Long.toString((timerange % 60)) + "s ");
            tvlasttimerange.setTextColor(Color.WHITE);
            tvlasttimerange.setGravity(Gravity.CENTER);
            tvlasttimerange.setBackgroundResource(R.drawable.cell_shape2);
            tvlasttimerange.setTextSize(txtSize);
            tbrow.addView(tvlasttimerange);
            TextView tvid = new TextView(this);
            tvid.setText(" " + values[10] + " ");
            tvid.setTextColor(Color.WHITE);
            tvid.setTextSize(txtSize + 1);
            tvid.setBackgroundResource(R.drawable.cell_shape);
            tbrow.addView(tvid);
            tbrow.setId(Integer.parseInt(values[10]));

            TextView tvname = new TextView(this);
            tvname.setText(" " + values[1] + " ");
            tvname.setTextColor(Color.WHITE);
            tvname.setGravity(Gravity.CENTER);
            tvname.setBackgroundResource(R.drawable.cell_shape2);
            tvname.setTextSize(txtSize);
            tbrow.addView(tvname);
            tvname.setVisibility(View.INVISIBLE);

            table.addView(tbrow);
        }
        Log.d(TAG, "Table Updated.");
    }

    public void updateRow(Map<String, String[]> map, String mac, int id) {
        Log.d(TAG, "Updating row of MAC: " + mac + ".");
        TableRow row1 = (TableRow) table.findViewById(id);
        int nIndex = table.indexOfChild(row1);
        table.removeView(row1); // invisible and height == 0
        TableRow row = new TableRow(this);
        String[] values = map.get(mac);
        TextView status = new TextView(this);
        status.setText(" " + values[0] + " ");
        status.setTextColor(Color.WHITE);
        status.setGravity(Gravity.CENTER);
        status.setBackgroundResource(R.drawable.cell_shape2);
        status.setTextSize(txtSize);
        row.addView(status);
        TextView mac1 = new TextView(this);
        mac1.setText(" " + mac + " ");
        mac1.setTextColor(Color.WHITE);
        mac1.setGravity(Gravity.CENTER);
        mac1.setBackgroundResource(R.drawable.cell_shape2);
        mac1.setTextSize(txtSize);
        row.addView(mac1);
        TextView tvname = new TextView(this);
        tvname.setText(" " + values[1] + " ");
        tvname.setTextColor(Color.WHITE);
        tvname.setGravity(Gravity.CENTER);
        tvname.setBackgroundResource(R.drawable.cell_shape2);
        tvname.setTextSize(txtSize);
        row.addView(tvname);
        TextView tvdevicename = new TextView(this);
        tvdevicename.setText(" " + values[2] + " ");
        tvdevicename.setTextColor(Color.WHITE);
        tvdevicename.setGravity(Gravity.CENTER);
        tvdevicename.setBackgroundResource(R.drawable.cell_shape2);
        tvdevicename.setTextSize(txtSize);
        row.addView(tvdevicename);
        TextView tvtimerange = new TextView(this);
        Long time = Long.parseLong(values[3].split(",", 2)[0]) / 1000;
        tvtimerange.setText(" " + Long.toString((time / 60) % 60) + "m-" + Long.toString((time % 60)) + "s ");
        tvtimerange.setTextColor(Color.WHITE);
        tvtimerange.setGravity(Gravity.CENTER);
        tvtimerange.setBackgroundResource(R.drawable.cell_shape2);
        tvtimerange.setTextSize(txtSize);
        row.addView(tvtimerange);
        TextView tvtimesdetected = new TextView(this);
        tvtimesdetected.setText(" " + values[4] + " ");
        tvtimesdetected.setTextColor(Color.WHITE);
        tvtimesdetected.setGravity(Gravity.CENTER);
        tvtimesdetected.setBackgroundResource(R.drawable.cell_shape2);
        tvtimesdetected.setTextSize(txtSize);
        row.addView(tvtimesdetected);
        TextView tvoverall = new TextView(this);
        Long cumulative = Long.parseLong(values[5].split(",", 2)[0]) / 1000;
        tvoverall.setText(" " + Long.toString((cumulative / 3600) / 24) + "h-" + Long.toString((cumulative / 60) % 60) + "m-" + Long.toString(cumulative % 60) + "s" + " ");
        tvoverall.setTextColor(Color.WHITE);
        tvoverall.setGravity(Gravity.CENTER);
        tvoverall.setBackgroundResource(R.drawable.cell_shape2);
        tvoverall.setTextSize(txtSize);
        row.addView(tvoverall);
        TextView tvlastdetecetd = new TextView(this);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy 'at' h:mm a");
        Date resultdate = new Date(Long.parseLong(values[6].split(",", 2)[0]));
        tvlastdetecetd.setText(" " + sdf.format(resultdate) + " ");
        tvlastdetecetd.setTextColor(Color.WHITE);
        tvlastdetecetd.setGravity(Gravity.CENTER);
        tvlastdetecetd.setBackgroundResource(R.drawable.cell_shape2);
        tvlastdetecetd.setTextSize(txtSize);
        row.addView(tvlastdetecetd);
        TextView tvlasttimerange = new TextView(this);
        Long timerange = Long.parseLong(values[7]) / 1000;
        tvlasttimerange.setText(" " + Long.toString((timerange / 60) % 60) + "m-" + Long.toString((timerange % 60)) + "s ");
        tvlasttimerange.setTextColor(Color.WHITE);
        tvlasttimerange.setGravity(Gravity.CENTER);
        tvlasttimerange.setBackgroundResource(R.drawable.cell_shape2);
        tvlasttimerange.setTextSize(txtSize);
        row.addView(tvlasttimerange);
        TextView tvid = new TextView(this);
        tvid.setText(" " + values[10] + " ");
        tvid.setTextColor(Color.WHITE);
        tvid.setTextSize(txtSize + 1);
        tvid.setBackgroundResource(R.drawable.cell_shape);
        row.addView(tvid);
        row.setId(id);
        // add row into same place
        table.addView(row, nIndex); // visible
        Log.d(TAG, "Row of MAC: " + mac + " updated.");
    }

    public void getPhoneNumber() {
        if(myphoneNumber != null) {
            if (myphoneNumber.length() != 0) {
                TextView v = (TextView) findViewById(R.id.my_phone_number);
                phoneNumber = myphoneNumber;
                v.setText("Phone Number: " + ClientActivity.myphoneNumber);
            } else {

                phoneNumber = new String("-");
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Input your phone number").setMessage("You either have to insert a SIM Card, or TelephonyManager.getLine1Number() is disabled on your phone");

                // Set up the input
                final EditText input = new EditText(this);
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_PHONE);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String nb = input.getText().toString();
                        if (nb.length() != 0)
                            phoneNumber = nb;
                        else phoneNumber = "-";

                        TextView v = (TextView) findViewById(R.id.my_phone_number);
                        v.setText("Phone Number: " + ClientActivity.phoneNumber);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        } else {

            phoneNumber = new String("-");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Input your phone number");

            // Set up the input
            final EditText input = new EditText(this);
            // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            input.setInputType(InputType.TYPE_CLASS_PHONE);
            builder.setView(input);

            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String nb = input.getText().toString();
                    if (nb.length() != 0)
                        phoneNumber = nb;
                    else phoneNumber = "-";

                    TextView v = (TextView) findViewById(R.id.my_phone_number);
                    v.setText("Phone Number: " + ClientActivity.phoneNumber);
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.show();
        }
    }
}