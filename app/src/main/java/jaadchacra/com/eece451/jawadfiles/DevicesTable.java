package jaadchacra.com.eece451.jawadfiles;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import jaadchacra.com.eece451.R;

/**
 * A Table of devices that displays all peers on discovery and requests the
 * parent activity to handle user interaction events
 */
public class DevicesTable extends Fragment implements PeerListListener {

    protected static List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
    static ProgressDialog progressDialog = null;
    View mContentView = null;
    private WifiP2pDevice device;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContentView = inflater.inflate(R.layout.table, null);
        return mContentView;
    }

    /**
     * @return this device
     */
    public WifiP2pDevice getDevice() {
        return device;
    }

    private static String getDeviceStatus(int deviceStatus) {
        Log.d(ClientActivity.TAG, "Peer status :" + deviceStatus);
        switch (deviceStatus) {
            case WifiP2pDevice.AVAILABLE:
                return "Available";
            case WifiP2pDevice.INVITED:
                return "Invited";
            case WifiP2pDevice.CONNECTED:
                return "Connected";
            case WifiP2pDevice.FAILED:
                return "Failed";
            case WifiP2pDevice.UNAVAILABLE:
                return "Unavailable";
            default:
                return "Unknown";
        }
    }

    /**
     * Update UI for this device.
     *
     * @param device WifiP2pDevice object
     */
    public void updateThisDevice(WifiP2pDevice device) {
        this.device = device;
        TextView view = (TextView) mContentView.findViewById(R.id.my_name);
        view.setText("Name: " + device.deviceName);
        view = (TextView) mContentView.findViewById(R.id.my_mac);
        view.setText("MAC: " + device.deviceAddress);
        view = (TextView) mContentView.findViewById(R.id.my_phone_number);
        view.setText("Phone Number: " + ClientActivity.phoneNumber);
    }

    public void clearPeers() {
        peers.clear();
        // ((WiFiPeerListAdapter) super.getListAdapter()).notifyDataSetChanged();
    }

    /**
     *
     */
    public void onInitiateDiscovery() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
       /* progressDialog = ProgressDialog.show(getActivity(), "Press back to cancel", "Finding peers", true,
                true, new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });*/
    }

    @Override
    public void onPeersAvailable(WifiP2pDeviceList newPeers) {
        ClientActivity x = (ClientActivity) ClientActivity.context;
        if (!x.wifiP2pScanThread.isScanning()) {
            Log.d(ClientActivity.TAG, "Scanning off: Broadcast receiver receiving false actions.");
            return;
        }
        ArrayList<WifiP2pDevice> newDevices = new ArrayList<>(newPeers.getDeviceList());
        peers.addAll(newPeers.getDeviceList());
        ArrayList<String> onlinemacs = new ArrayList<>();
        for (WifiP2pDevice dev : peers) {
            Log.d(ClientActivity.TAG, "Address " + dev.deviceAddress.toString());
            onlinemacs.add(dev.deviceAddress.toString());
        }
        if (peers.size() == 0) {
            Log.d(ClientActivity.TAG, "No devices found");
        } else {
            for (WifiP2pDevice dev : peers) {
                String mac = dev.deviceAddress.toString();
                onlinemacs.add(mac);
                if (!x.detectedDevices.containsKey(mac)) { // discovering new device
                    String[] values = new String[]{"In-range", "", dev.deviceName, "0, ", "1", "0", Long.toString(System.currentTimeMillis()), "0", Long.toString(System.currentTimeMillis()), "0", Integer.toString(x.detectedDevices.size() + 1)};
                    x.detectedDevices.put(mac, values);
                    Set<String> s = new TreeSet<>();
                    s.add(mac);
                    x.updateTable(x.detectedDevices, s);
                } else if (x.detectedDevices.containsKey(mac) && onlinemacs.contains(mac) && x.detectedDevices.get(mac)[0].equalsIgnoreCase("out of range")) { // device went from offline to online
                    String values[] = x.detectedDevices.get(mac);
                    values[0] = "In-range"; //status
                    values[7] = values[3].split(",", 2)[0];
                    Log.d(x.TAG, "values " + values[7]);
                    values[3] = "0," + values[3]; //start recording new detection time
                    values[4] = Integer.toString(Integer.parseInt(values[4]) + 1);
                    values[6] = System.currentTimeMillis() + "," + values[6]; //new last date
                    values[8] = Long.toString(System.currentTimeMillis());
                    x.detectedDevices.put(mac, values);
                    x.updateRow(x.detectedDevices, mac, Integer.parseInt(values[10]));
                } else if (x.detectedDevices.containsKey(mac) && onlinemacs.contains(mac)) { // device already online
                    String[] values = x.detectedDevices.get(mac);
                    Long now = System.currentTimeMillis(); //recording detection time
                    Long recording = now - Long.parseLong(values[8]);
                    String parts[] = values[3].split(",", 2);
                    parts[0] = Long.toString(recording);
                    values[3] = parts[0] + "," + parts[1];
                    values[5] = Long.toString(Long.parseLong(values[5]) + recording); //adding cumulative detection time
                    x.updateRow(x.detectedDevices, mac, Integer.parseInt(values[10]));
                }
            }
        }
        save();
    }

    /**
     * An interface-callback for the activity to listen to fragment interaction
     * events.
     */
    public interface DeviceActionListener {

        void showDetails(WifiP2pDevice device);

        void cancelDisconnect();

        void connect(WifiP2pConfig config);

        void disconnect();
    }

    public static void save() {
        try {
            String data = ClientActivity.phoneNumber + "\n";
            data += ClientActivity.device.deviceAddress.toString() + "\n";
            Set<String> onkeys = ClientActivity.detectedDevices.keySet();
            if(onkeys.size()>0){
                for (String key : onkeys) {
                    data += key + "\n";
                    String[] values = ClientActivity.detectedDevices.get(key);
                    for (String value : values) {
                        data += value + "\n";
                    }
                }
            }
            // Creates a trace file in the primary external storage space of the
            // current application.
            // If the file does not exists, it is created.
            File traceFile = new File(ClientActivity.context.getExternalFilesDir(null), "p2p-info.txt");
            if (!traceFile.exists())
                traceFile.createNewFile();
            // Adds a line to the trace file
            BufferedWriter writer = new BufferedWriter(new FileWriter(traceFile, false));
            writer.write(data);
            writer.close();
            Log.d(ClientActivity.TAG, "File saved successfully.");
            // Refresh the data so it can seen when the device is plugged in a
            // computer. You may have to unplug and replug the device to see the
            // latest changes. This is not necessary if the user should not modify
            // the files.
            //   MediaScannerConnection.scanFile(ClientActivity.context,
            //         new String[]{traceFile.toString()},
            //       null,
            //     null);
        } catch (IOException e) {
            Log.e(ClientActivity.TAG, "Unable to write to the p2p-info.txt file.");
        }
    }
}