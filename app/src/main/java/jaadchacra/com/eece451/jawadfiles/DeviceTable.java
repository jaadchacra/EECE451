package jaadchacra.com.eece451.jawadfiles;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import jaadchacra.com.eece451.R;

/**
 * A ListFragment that displays available peers on discovery and requests the
 * parent activity to handle user interaction events
 */
public class DeviceTable extends Fragment implements PeerListListener {

    protected static List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
    static ProgressDialog progressDialog = null;
    View mContentView = null;
    private WifiP2pDevice device;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //this.setListAdapter(new WiFiPeerListAdapter(getActivity(), R.layout.table, peers));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContentView = inflater.inflate(R.layout.table, null);
        return mContentView;
    }

    /**
     * @return this device
     */
    public WifiP2pDevice getDevice() {
        return device;
    }

    private static String getDeviceStatus(int deviceStatus) {
        Log.d(ClientActivity.TAG, "Peer status :" + deviceStatus);
        switch (deviceStatus) {
            case WifiP2pDevice.AVAILABLE:
                return "Available";
            case WifiP2pDevice.INVITED:
                return "Invited";
            case WifiP2pDevice.CONNECTED:
                return "Connected";
            case WifiP2pDevice.FAILED:
                return "Failed";
            case WifiP2pDevice.UNAVAILABLE:
                return "Unavailable";
            default:
                return "Unknown";
        }
    }

    /**
     * Update UI for this device.
     *
     * @param device WifiP2pDevice object
     */
    public void updateThisDevice(WifiP2pDevice device) {
        this.device = device;
        TextView view = (TextView) mContentView.findViewById(R.id.my_name);
        view.setText("Name: " + device.deviceName);
        view = (TextView) mContentView.findViewById(R.id.my_mac);
        view.setText("MAC: " + device.deviceAddress);
        view = (TextView) mContentView.findViewById(R.id.my_status);
        view.setText(getDeviceStatus(device.status));
    }

    public void clearPeers() {
        //peers.clear();
        // ((WiFiPeerListAdapter) super.getListAdapter()).notifyDataSetChanged();
    }

    /**
     *
     */
    public void onInitiateDiscovery() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
       /* progressDialog = ProgressDialog.show(getActivity(), "Press back to cancel", "Finding peers", true,
                true, new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });*/
    }

    @Override
    public void onPeersAvailable(WifiP2pDeviceList newPeers) {
        ClientActivity x = (ClientActivity) ClientActivity.context;
        if(!x.wifiP2pScanThread.isScanning()){
            Log.d(ClientActivity.TAG, "Scanning out of control. Broadcast receiver receiving false actions.");
            return;
        }
        peers.addAll(newPeers.getDeviceList());
        Set<String> newDevs;
        if (peers.size() == 0) {
            Log.d(ClientActivity.TAG, "No devices found");
            return;
        } else {
            newDevs = new TreeSet<>();
            for (WifiP2pDevice dev : peers) {
                String mac = dev.deviceAddress.toString();
                String[] values;
                if (x.detectedDevices.containsKey(mac)) {
                    values = x.detectedDevices.get(mac);
                    if (x.devsOn.contains(mac)) {
                        Long now = System.currentTimeMillis();
                        Long diff = (now - Long.parseLong(values[7])) / 1000;
                        Long cumulative = (now - Long.parseLong(values[8]))/1000 + diff;
                        values[2] = Long.toString((diff / 60)) + "m" + Long.toString((diff % 60)) + "s";
                        values[4] = Long.toString((cumulative / 360)) + "h" + Long.toString((cumulative / 60)) + "m" + Long.toString((cumulative % 60)) + "s";
                        values[5] = Calendar.getInstance().getTime().toString();
                        Toast.makeText(x, "e"+values[4],
                                Toast.LENGTH_SHORT).show();
                        continue;
                    } else {
                        Toast.makeText(x, "de"+values[4],
                                Toast.LENGTH_SHORT).show();
                        values[6] = values[2];
                        values[2] = "0s";
                        values[3] = Integer.toString(Integer.parseInt(values[3]) + 1); //increment the number of times detected
                        values[4] = "0s";
                        values[5] = Calendar.getInstance().getTime().toString();
                        values[7] = Long.toString(System.currentTimeMillis());
                        values[8] = values[7];
                        x.devsOn.add(mac);
                    }
                    x.detectedDevices.put(mac, values);
               //     x.updateRow(mac, Integer.parseInt(values[9]));
                } else {
                    String key = (mac);
                    values = new String[]{"", dev.deviceName, "0", "1", "0", Calendar.getInstance().getTime().toString(), "0", Long.toString(System.currentTimeMillis()), "0", Integer.toString(ClientActivity.detectedDevices.size() + 1)};
                    x.detectedDevices.put(key, values);
                    newDevs.add(key);
                }
            }
            List<WifiP2pDevice> temp = new ArrayList<>();
            temp.addAll(newPeers.getDeviceList());
            ClientActivity.devsOn.clear();
            for (WifiP2pDevice dev : temp) {
                ClientActivity.devsOn.add(dev.deviceAddress.toString());
            }
        }
      //  x.updateTable(newDevs);
        save();
    }

    /**
     * An interface-callback for the activity to listen to fragment interaction
     * events.
     */
    public interface DeviceActionListener {

        void showDetails(WifiP2pDevice device);

        void cancelDisconnect();

        void connect(WifiP2pConfig config);

        void disconnect();
    }

    public void save() {
        try {
            String data = "";
            Set<String> keys = ClientActivity.detectedDevices.keySet();
            for (String key : keys) {
                data += key + "\n";
                String[] values = ClientActivity.detectedDevices.get(key);
                for (String value : values) {
                    data += value + "\n";
                }
            }
            // Creates a trace file in the primary external storage space of the
            // current application.
            // If the file does not exists, it is created.
            File traceFile = new File(ClientActivity.context.getExternalFilesDir(null), "p2p-info.txt");
            if (!traceFile.exists())
                traceFile.createNewFile();
            // Adds a line to the trace file
            BufferedWriter writer = new BufferedWriter(new FileWriter(traceFile, false));
            writer.write(data);
            writer.close();
            Log.d(ClientActivity.TAG, "File saved successfully.");
            // Refresh the data so it can seen when the device is plugged in a
            // computer. You may have to unplug and replug the device to see the
            // latest changes. This is not necessary if the user should not modify
            // the files.
            //   MediaScannerConnection.scanFile(ClientActivity.context,
            //         new String[]{traceFile.toString()},
            //       null,
            //     null);

        } catch (IOException e) {
            Log.e(ClientActivity.TAG, "Unable to write to the p2p-info.txt file.");
        }
    }
}